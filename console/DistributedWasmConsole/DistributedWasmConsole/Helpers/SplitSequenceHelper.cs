﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using DistributedWasmConsole.Models;

namespace DistributedWasmConsole.Helpers
{
    public static class SplitSequenceHelper
    {
        public static List<string> SplitSequence(string sequence, byte atomSize)
        {
            var sequenceLength = sequence.Length;
            var atomCount = sequenceLength - atomSize + 1;
            var result = new List<string>(atomCount);

            for (int i = 0; i < atomCount; i++)
            {
                result.Add(sequence.Substring(i, atomSize));
            }

            var resultDistinct = result.Distinct().ToList();


            return resultDistinct;
        }

        public static string SequenceFromConnections(IEnumerable<Connection> connections)
        {
            var builder = new StringBuilder(connections.First().From.Atom);

            foreach (var connection in connections)
            {
                builder.Append(connection.To.Atom.Substring(connection.Score));
            }

            return builder.ToString();
        }
    }
}
