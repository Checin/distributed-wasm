﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoreLinq;

namespace DistributedWasmConsole.Models
{
    public class Graph
    {
        private Graph() { }

        public Vertex[] Vertices { get; set; }

        public Vertex StartVertex { get; set; }

        public int TargetLength { get; set; }

        public static Graph FromSplitSequence(IEnumerable<string> sequence, int targetLength)
        {
            var result = new Graph();

            result.Vertices = sequence.Select(a => new Vertex { Atom = a, Graph = result }).ToArray();
            result.TargetLength = targetLength;

            foreach (var vertex in result.Vertices.Index())
            {
                vertex.Value.Id = vertex.Key;
                vertex.Value.SetConnections();
            }

            return result;
        }
    }
}
