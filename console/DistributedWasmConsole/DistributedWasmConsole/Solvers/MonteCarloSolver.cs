﻿using DistributedWasmConsole.Models;
using MoreLinq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DistributedWasmConsole.Solvers
{
    public class MonteCarloSolver
    {
        private readonly int _maxDepth;
        private readonly Connection _firstConnection;
        private readonly int _missingLettersCount;
        private readonly Random _randomInstance;
        private readonly int[] _usedArray;

        private MonteCarloSolver(Connection firstConnection, int missingLettersCount, int[] usedArray, int maxDepth)
        {
            _firstConnection = firstConnection;
            _missingLettersCount = missingLettersCount;
            _usedArray = usedArray;
            _maxDepth = maxDepth;
            _randomInstance = new Random();
        }

        private int GetScore(int iterationCount)
        {
            var score = 0;

            for (int i = 0; i < iterationCount; ++i)
            {
                score += GetSingleSearchScore();
            }

            return score;
        }

        private int GetSingleSearchScore()
        {
            var currentConnection = _firstConnection;
            var matchedLetters = 0;
            var score = _firstConnection.Score - (CurrentUsageCount(_usedArray, _firstConnection.To));

            while (matchedLetters < _missingLettersCount && matchedLetters < _maxDepth)
            {
                var currentVertex = currentConnection.To;
                var connections = currentVertex.Connections;

                if (connections.Length == 0)
                {
                    break;
                }

                var nextConnection = connections[_randomInstance.Next(connections.Length)];
                matchedLetters += currentVertex.Atom.Length - nextConnection.Score;
                score += nextConnection.Score - (CurrentUsageCount(_usedArray, nextConnection.To));
                currentConnection = nextConnection;
            }

            return score;
        }

        public static Connection GetNextConnection(Vertex currentVertex, int missingLettersCount, int iterationCount, int[] usedArray, int maxDepth)
        {
            var connections = currentVertex.Connections.Where(c => currentVertex.Atom.Length - c.Score <= missingLettersCount).ToArray();

            if (connections.Length == 0)
            {
                connections = currentVertex.Connections.GroupBy(c => c.Score).OrderBy(g => g.Key).FirstOrDefault()?.ToArray();

                if (connections == null)
                {
                    return new Connection();
                }
            }

            var scores = new int[connections.Length];

            foreach (var connection in connections.Index())
            {
                scores[connection.Key] = new MonteCarloSolver(connection.Value, missingLettersCount, usedArray, maxDepth).GetScore(iterationCount);
            }

            var index = scores.Index().MaxBy(s => s.Value).Last().Key;

            return connections[index];
        }

        public static List<Connection> GetSolution(Graph graph, int iterationCount, int maxDepth)
        {
            var missingLettersCount = graph.TargetLength - graph.StartVertex.Atom.Length;
            var currentVertex = graph.StartVertex;
            var result = new List<Connection>();
            var usedArray = new int[graph.Vertices.Length];

            usedArray[currentVertex.Id] = 1;

            while (missingLettersCount > 0)
            {
                var nextConnection = GetNextConnection(currentVertex, missingLettersCount, iterationCount, usedArray, maxDepth);

                if (nextConnection.To == null)
                {
                    break;
                }

                result.Add(nextConnection);
                currentVertex = nextConnection.To;
                ++usedArray[currentVertex.Id];
                missingLettersCount -= currentVertex.Atom.Length - nextConnection.Score;
            }

            return result;
        }

        private static int CurrentUsageCount(int[] usedArray, Vertex vertex)
        {
            return usedArray[vertex.Id];
        }
    }
}
