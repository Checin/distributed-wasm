const importObj = {
    module: {},
    env: {
      memory: new WebAssembly.Memory({ initial: 256 }),
      table: new WebAssembly.Table({ initial: 0, element: 'anyfunc' }),
      abort: () => null,
    }
  };
  
  WebAssembly.instantiateStreaming(fetch('./build/optimized.wasm'), importObj)
    .then(wasmModule => {
      const exports = wasmModule.instance.exports;
      const mem = new Uint32Array(exports.memory.buffer);
  
      console.log('start calculation');
      console.time('pi');
      console.log(exports.calculatePi());
      console.timeEnd('pi');
    });