#include <algorithm>
#include <memory>

#include "Graph.hpp"

namespace SBH
{
    std::shared_ptr<Graph> Graph::FromSplitSequence(const std::vector<std::string> &sequence, int targetLength, std::string startingVertex)
    {
        auto result = std::make_shared<Graph>();

        for (auto s : sequence)
        {
            Vertex vertex;
            vertex.SetAtom(s);
            vertex.SetGraph(result);
            auto vPointer = std::make_shared<Vertex>(vertex);
            result->vertices_.emplace_back(vPointer);

            if (s.compare(startingVertex) == 0)
            {
                result->startVertex_ = vPointer;
            }
        }

        result->targetLength_ = targetLength;

        for (auto index = 0u; index < result->vertices_.size(); index++)
        {
            auto vertex = result->vertices_[index];
            vertex->SetId(static_cast<long>(index));
            vertex->SetConnections();
        }

        return result;
    }

    const std::shared_ptr<Vertex> &Graph::GetStartVertex() const
    {
        return startVertex_;
    }

    int Graph::GetTargetLength() const
    {
        return targetLength_;
    }

    const std::vector<std::shared_ptr<Vertex>> &Graph::GetVertices() const
    {
        return vertices_;
    }
} // namespace SBH