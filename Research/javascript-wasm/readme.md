# AssemblyScript Monte Carlo Pi Calculation
AssemblyScript compiles a strict subset of TypeScript (basically JavaScript with types) to WebAssembly using Binaryen.

## Project structure
* /assembly - contains typescript that'll be compiled into WebAssembly - index.ts is the entry file
* /build - will contain WebAssembly build results
* index.html - main document of the project, entry point
* index.js - contains logic for built wasm file fetch and execution

## How to build
* Make sure you have Node.js and npm installed
* Install TypeScript support `npm i typescript -g`
* Install AssemblyScript `npm install --save-dev assemblyscript` (run command from scope of this project directory)
* `npm install`
*  Run build `npm run asbuild`

## How to run
Most browsers don't allow to `fetch()` from local filesystem, so you need to host this project files on some server:
* You can use IIS if you're running Windows
* You can try to open this directory in Visual Studio and hit 'run' - it should temporary host the project on IIS Express and open it in the browser

Open dev tools console (F12) for results