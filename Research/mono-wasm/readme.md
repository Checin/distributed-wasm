# Mono Wasm Monte Carlo Pi Calculation
Basic implementation for Monte Carlo Pi Calculation in .NET

## Project structure
* sample.html - entry point
* wasm-test.cpp - all the logic

## How to build
* Download mono-wasm build, extract to any directory and set WASM_SDK env variable to it, example: `export WASM_SDK=~/Documents/mono-wasm`
* Build: `csc /target:library -out:sample.dll /noconfig /nostdlib /r:$WASM_SDK/wasm-bcl/wasm/mscorlib.dll /r:$WASM_SDK/wasm-bcl/wasm/System.dll /r:$WASM_SDK/wasm-bcl/wasm/System.Core.dll /r:$WASM_SDK/wasm-bcl/wasm/Facades/netstandard.dll /r:$WASM_SDK/wasm-bcl/wasm/System.Net.Http.dll /r:$WASM_SDK/framework/WebAssembly.Bindings.dll /r:$WASM_SDK/framework/WebAssembly.Net.WebSockets.dll sample.cs`
* Package: `mono $WASM_SDK/packager.exe --copy=always --out=./publish --asset=./sample.html sample.dll`

## How to run
Most browsers don't allow to `fetch()` from local filesystem, so you need to host this project files on some server:
* You can use IIS if you're running Windows
* You can try to open this directory in Visual Studio and hit 'run' - it should temporary host the project on IIS Express and open it in the browser
* If you have Python 2 installed, just run `python ./publish/server.py`
