# C++ Monte Carlo Pi Calculation
Basic implementation for Monte Carlo Pi Calculation in C++ 

## Project structure
* index.html - entry point
* wasm-test.cpp - all the logic

## How to build
* Make sure you have emscripten installed
* `emcc -O3 --bind -o main.js wasm-test.cpp -s WASM=1`

## How to run
Most browsers don't allow to `fetch()` from local filesystem, so you need to host this project files on some server:
* You can use IIS if you're running Windows
* You can try to open this directory in Visual Studio and hit 'run' - it should temporary host the project on IIS Express and open it in the browser

Open dev tools console (F12) for results