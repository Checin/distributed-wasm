function calculatePi() {
    console.log('Start');

    var inner = 0;
    var count = 0;

    for (var i = 0; i < 300000000; i ++) {
        var x = Math.random();
        var y = Math.random();

        var distance = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));

        if (distance <= 1) {
            inner++;
        }
        count ++;
    }
    
    var result = 4 * inner / count;

    console.log(result);

    console.log('Done');
}

console.time('pi');
calculatePi();
console.timeEnd('pi');