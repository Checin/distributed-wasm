#include "MonteCarloSolver.hpp"
#include <limits>
#include <algorithm>

namespace SBH
{
    MonteCarloSolver::MonteCarloSolver(Connection firstConnection, int missingLettersCount,
                                       const std::vector<int> &usedArray, int maxDepth, int seed)
        : firstConnection_(firstConnection), missingLettersCount_(missingLettersCount), usedArray_(usedArray), maxDepht_(maxDepth)
    {
    }

    int MonteCarloSolver::GetScore(int iterationCount) const
    {
        auto score = 0;
        for (int i = 0;  i < iterationCount; i++)
        {
            score += GetSingleSearchScore();
        }

        return score;
    }

    int MonteCarloSolver::GetSingleSearchScore() const
    {
        auto currentConnection = firstConnection_;
        auto matchedLetters = 0;
        auto score = firstConnection_.Score() - (CurrentUsageCount(usedArray_, firstConnection_.To()));

        while (matchedLetters < missingLettersCount_ && matchedLetters < maxDepht_)
        {
            const auto& currentVertex = currentConnection.To();
            const auto& connections = currentVertex->GetConnections();

            if (connections.size() == 0)
            {
                break;
            }

            auto randomIndex = Helper::GenerateRandomNumber(0, static_cast<int>(connections.size()) - 1);
            auto &nextConnection = connections[randomIndex];
            matchedLetters += currentVertex->GetAtom().length();
            score += nextConnection.Score() - (MonteCarloSolver::CurrentUsageCount(usedArray_, (nextConnection.To())));
            currentConnection = nextConnection;
        }

        return score;
    }

    Connection MonteCarloSolver::GetNextConnection(const std::shared_ptr<Vertex>& currentVertex, int missingLettersCount,
                                                          int iterationCount, const std::vector<int> &usedArray, int maxDepth)
    {
        std::vector<Connection> connections;
        std::copy_if(currentVertex->GetConnections().begin(), currentVertex->GetConnections().end(), std::back_inserter(connections),
                     [&](const Connection &c) {
                         return (currentVertex->GetAtom().length() - c.Score()) <= missingLettersCount;
                     });
        
        if (connections.size() == 0)
        {
            return Connection();
        }

        std::vector<int> scores(connections.size());

        for (auto index = 0u; index < scores.size(); index++)
        {
            auto randomSeed = Helper::GenerateRandomNumber(0, std::numeric_limits<int>::max());
            auto x = MonteCarloSolver(connections[index], missingLettersCount, usedArray, maxDepth, randomSeed);
            scores[index] = x.GetScore(iterationCount);
        }

        auto maxIndexIterator = std::max_element(scores.begin(), scores.end());
        auto index = std::distance(scores.begin(), maxIndexIterator);

        return connections[index];
    }

    std::vector<Connection> MonteCarloSolver::GetSolution(std::shared_ptr<Graph>& graph, int iterationCount, int maxDepth)
    {
        auto missingLettersCount = graph->GetTargetLength() - graph->GetStartVertex()->GetAtom().length();
        auto currentVertex = graph->GetStartVertex();
        auto result = std::vector<Connection>();
        auto usedArray = std::vector<int>(graph->GetVertices().size());

        usedArray[currentVertex->GetId()] = 1;

        while (missingLettersCount > 0)
        {
            auto nextConnection = GetNextConnection(currentVertex, missingLettersCount, iterationCount, usedArray, maxDepth);

            if (nextConnection.To() == nullptr)
            {
                break;
            }

            result.emplace_back(nextConnection);
            currentVertex = nextConnection.To();
            ++usedArray[currentVertex->GetId()];
            missingLettersCount -= currentVertex->GetAtom().length() - nextConnection.Score();
        }

        return result;
    }

    int MonteCarloSolver::CurrentUsageCount(const std::vector<int>& usedArray, const std::shared_ptr<Vertex>& vertexPtr)
    {
        return usedArray[vertexPtr->GetId()];
    }

} // namespace SBH