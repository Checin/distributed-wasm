import init from "./pkg/rust_pi_wasm.js"

let rustWasm;

const initWasm = async () => {
    rustWasm =  await init();
}

initWasm();

// js rand - fatster version
export function calculatePiRust() {
    return rustWasm.monte_carlo_js_rand();
}

// rust rand - slower version
export function calculatePiRurstRandCrate()
{
    return runWasm.monte_carlo_rust_rand();
}