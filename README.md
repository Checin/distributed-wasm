# Przetwarzanie rozproszone dużej skali z wykorzystaniem nowoczesnych technologii przeglądarkowych #

### Zawartość ###

* client - aplikacja kliencka
* console - aplikacja desktopowa
* research - implementacje algorytmu testowego w różnych technologiach, kod strony internetowej agregującej
* server - aplikacja serwerowa i panel administracyjny
* tests - skrypty pomocne przy przeprowadzaniu testów dużej skali
* Wyniki.xlsx - wyniki testów