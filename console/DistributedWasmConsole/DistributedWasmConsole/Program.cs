﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DistributedWasmConsole.Helpers;
using DistributedWasmConsole.Models;
using DistributedWasmConsole.Solvers;

namespace DistributedWasmConsole
{
    class Program
    {
        private static int _sequenceLength = 1000;
        private static byte _oligonucleotydeSize = 10;
        private static int _iterationCount = 50;
        private static int _maxDepth = 50;

        static void Main(string[] args)
        {
            // server
            var sequence = GenerateSequenceHelper.GenerateSequence(_sequenceLength);
            var splitSequence = SplitSequenceHelper.SplitSequence(sequence, _oligonucleotydeSize);

            var swTotal = Stopwatch.StartNew();

            var solutions = new List<(int, string)>();

            // client
            var graph = Graph.FromSplitSequence(splitSequence, _sequenceLength);

            foreach (var vertex in graph.Vertices)
            {
                Console.WriteLine($"VertexId: {vertex.Id}");

                graph.StartVertex = vertex;

                var solution = MonteCarloSolver.GetSolution(graph, _iterationCount, _maxDepth);

                // server
                var percentage = SolutionQualityHelper.GetUsedVerticesPercentage(solution, graph);
                solutions.Add((percentage, SplitSequenceHelper.SequenceFromConnections(solution)));
            }

            // server
            var mostPromisingSolutions = solutions.GroupBy(s => s.Item1).OrderByDescending(s => s.Key).First();

            swTotal.Stop();

            Console.WriteLine($"Total elapsed: {swTotal.ElapsedMilliseconds}");
        }
    }
}
