# JavaScript Monte Carlo Pi Calculation
Base implementation for Monte Carlo Pi Calculation in Javascript which will be used as base for benchmarking other technologies

## Project structure
* index.html - entry point
* main.js - all the logic

## How to run
Just open index.html in web browser. Alternatively you can host it on any server.

Open dev tools console (F12) for results