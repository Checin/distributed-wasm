import wasmCpp from './wasm-test.js';

const instanceCPP = wasmCpp({
    onRuntimeInitialized() {
        console.log('start calculation');
        console.time('pi');
        console.log(instanceCPP.monteCarloPi(300000000));
        console.timeEnd('pi');
    }
});