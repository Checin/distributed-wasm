#include <vector>
#include <iostream>
#include <emscripten/bind.h>
#include <random>
#include <time.h>

double monte_carlo_pi()
{
    srand (time(NULL));
    double randMaxFloat = (double)RAND_MAX;

    int count = 0;
    int inner = 0;

    for (int i = 0; i < 300000000; ++i)
    {
        double x = rand() / randMaxFloat;
        double y = rand() / randMaxFloat;

        double distance = sqrt(pow(x, 2) + pow(y, 2));

        if (distance <= 1) {
            inner++;
        }

        count++;
    }


    return 4.0 * inner / count;
}

EMSCRIPTEN_BINDINGS(wasm_test) {
    emscripten::function("monteCarloPi", &monte_carlo_pi);
}