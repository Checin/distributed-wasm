﻿
namespace DistributedWasmConsole.Models
{
    public struct Connection
    {
        public Vertex To { get; }

        public Vertex From { get; }

        public int Score { get; }

        public Connection(Vertex from, Vertex to, int score)
        {
            To = to;
            From = from;
            Score = score;
        }

        public override string ToString()
        {
            return $"{From} => {To} ({Score})";
        }
    }
}
