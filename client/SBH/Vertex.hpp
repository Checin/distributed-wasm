#ifndef SBH_VERTEX_HPP
#define SBH_VERTEX_HPP

#include <algorithm>
#include <vector>
#include <iostream>
#include <string_view>
#include <memory>

#include "Graph.hpp"


namespace SBH
{
    class Graph;
    class Connection;
    class Vertex : public std::enable_shared_from_this<Vertex>
    {
        public:
            Vertex() = default;
            int ScoreFrom(const Vertex& from) const;
            long GetId() const;
            void SetId(long id);
            const std::string& GetAtom() const;
            void SetAtom(std::string atom);
            const std::vector<Connection>& GetConnections() const;
            void SetGraph(const std::shared_ptr<Graph>& graph);
            void SetConnections();
            void SetConnections(std::vector<Connection> connections);
            friend std::ostream& operator<<(std::ostream& os, const Vertex& vertex);
            std::shared_ptr<Vertex> GetPtr();

        private:
            std::shared_ptr<Graph> graph_;
            long id_;
            std::string atom_;
            std::vector<Connection> connections_;
    };
} // namespace SBH

#endif // SBH_VERTEX_HPP