/* tslint:disable */
/* eslint-disable */
/**
* @returns {number}
*/
export function monte_carlo_js_rand(): number;
/**
* @returns {number}
*/
export function monte_carlo_rust_rand(): number;

export type InitInput = RequestInfo | URL | Response | BufferSource | WebAssembly.Module;

export interface InitOutput {
  readonly memory: WebAssembly.Memory;
  readonly monte_carlo_js_rand: () => number;
  readonly monte_carlo_rust_rand: () => number;
  readonly __wbindgen_exn_store: (a: number) => void;
}

/**
* If `module_or_path` is {RequestInfo} or {URL}, makes a request and
* for everything else, calls `WebAssembly.instantiate` directly.
*
* @param {InitInput | Promise<InitInput>} module_or_path
*
* @returns {Promise<InitOutput>}
*/
export default function init (module_or_path?: InitInput | Promise<InitInput>): Promise<InitOutput>;
        