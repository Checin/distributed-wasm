mod utils;
use wasm_bindgen::prelude::*;
use js_sys::Math;


// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;


#[wasm_bindgen]
pub fn caculate_pi() -> f64
{
    let mut inner = 0;
    let mut count = 0;

    for _ in 0..300000000 {
        let x = Math::random();
        let y = Math::random();

        let distance = (x.powf(2.0) + y.powf(2.0)).sqrt();

        if distance <= 1.0 {
            inner += 1;
        }

        count += 1;
    }

    let result = 4.0 * inner as f64 / count as f64;

    result
}

