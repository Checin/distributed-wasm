#include "Helper.hpp"
#include "MonteCarloSolver.hpp"
#include <vector>
#include <iostream>
#include <emscripten/bind.h>
#include <string.h>
#include <vector>
#include <time.h>

std::vector<SBH::Connection> calculate(std::string startingVertex, int targetLength, std::vector<std::string> &vertices) {
    srand (time(NULL));
    auto graphPtr = SBH::Graph::FromSplitSequence(vertices, targetLength, startingVertex);
    auto solution = SBH::MonteCarloSolver::GetSolution(graphPtr, 50, 50);

    return solution;
}

EMSCRIPTEN_BINDINGS(wasm_run) {
    emscripten::register_vector<std::string>("StringVector");
    emscripten::function("calculate", &calculate);
    emscripten::class_<SBH::Connection>("Connection")
        .function("Score", &SBH::Connection::Score)
        .function("From2", &SBH::Connection::From2)
        .function("To2", &SBH::Connection::To2);
    emscripten::class_<SBH::Vertex>("Vertex")
        .property("Atom", &SBH::Vertex::GetAtom, &SBH::Vertex::SetAtom)
        .smart_ptr<std::shared_ptr<SBH::Vertex>>("Vertex");
    emscripten::register_vector<SBH::Connection>("ConnectionVector");
}