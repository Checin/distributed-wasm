#include "Vertex.hpp"

namespace SBH
{
    int Vertex::ScoreFrom(const Vertex &from) const
    {
        std::size_t i;
        auto fromAtom = from.atom_;
        auto atomLength = fromAtom.length();
        auto toAtom = atom_;


        for (i = 1u; i < toAtom.size(); ++i)
        {
            if (!(toAtom.find(fromAtom.substr(i, fromAtom.size())) == 0))
            {
                break;
            }
        }

        return static_cast<int>(atomLength - i);
    }

    long Vertex::GetId() const
    {
        return id_;
    }

    void Vertex::SetId(long id)
    {
        id_ = id;
    }

    const std::string& Vertex::GetAtom() const
    {
        return atom_;
    }

    void Vertex::SetAtom(std::string atom)
    {
        atom_ = std::move(atom);
    }

    const std::vector<Connection>& Vertex::GetConnections() const
    {
        return connections_;
    }

    void Vertex::SetConnections()
    {

        for (const auto& vertePtr : graph_->GetVertices())
        {
            int score = vertePtr->ScoreFrom(*this);

            if (score > 0)
            {
                connections_.emplace_back(shared_from_this(), vertePtr, score);
            }
        }
    }

    void Vertex::SetConnections(std::vector<Connection> connections)
    {
        connections_ = connections;
    }

    void Vertex::SetGraph(const std::shared_ptr<Graph>& graph)
    {
        graph_ = graph;
    }
    std::shared_ptr<Vertex> Vertex::GetPtr()
    {
        return shared_from_this();
    }

} // namespace SBH