import { calculatePiRust } from "./rust/main.js"

const executeAllButton = document.getElementById('execute-all-btn');
const executeJsButton = document.getElementById('execute-js-btn');
const executeAsButton = document.getElementById('execute-as-btn');
const executeCppButton = document.getElementById('execute-cpp-btn');
const executeRustButton = document.getElementById('execute-rust-btn');
const downloadResultsButton = document.getElementById('download-results-btn');
const stopButton = document.getElementById('stop-btn');

const assemblyScriptExecutionList = document.getElementById('assemblyscript-execution-list');
const javascriptExecutionList = document.getElementById('javascript-execution-list');
const cppExecutionList = document.getElementById('cpp-execution-list');
const rustExecutionList = document.getElementById('rust-execution-list');
const resultsSection = document.getElementById('results-section');

var isTryingToStop = false;

executeAllButton.onclick = async function(){
    isTryingToStop = false;
    var executionCount = document.getElementById('execution-count').value;
    startWorkForJavascript(executionCount);
    await startWorkForAssemblyscriptAsync(executionCount);
    startWorkForCpp(executionCount);
}
executeJsButton.onclick = function(){
    isTryingToStop = false;
    var executionCount = document.getElementById('execution-count').value;
    startWorkForJavascript(executionCount);
}
executeAsButton.onclick = async function(){
    isTryingToStop = false;
    var executionCount = document.getElementById('execution-count').value;
    await startWorkForAssemblyscriptAsync(executionCount);
}
executeCppButton.onclick = function(){
    isTryingToStop = false;
    var executionCount = document.getElementById('execution-count').value;
    startWorkForCpp(executionCount);
}

executeRustButton.onclick = function(){
    isTryingToStop = false;
    var executionCount = document.getElementById('execution-count').value;
    startWorkForRust(executionCount);
}

downloadResultsButton.onclick = function(){
    const a = document.createElement('a');
    a.style.display = 'none';
    var file = new Blob([resultsSection.innerText], {type: 'text/plain'});
    a.href = URL.createObjectURL(file);
    a.download = 'results.txt';
    document.body.appendChild(a);
    a.click();
}
stopButton.onclick = function(){
    isTryingToStop = true;
}

function startWorkForJavascript(executionCount) {
    console.log("START JS");
    for (let i = 0; i < executionCount; i++) {

        var t0 = performance.now();
        let result = calculatePiJavascript();
        var t1 = performance.now();
        appendListItem(javascriptExecutionList, (t1-t0), result);
        
        if(isTryingToStop){
            i = executionCount;
        }
    }
}

async function startWorkForAssemblyscriptAsync(executionCount) {
    console.log("START AS");
    for (let i = 0; i < executionCount; i++) {

        var t0 = performance.now();
        let result = await calculatePiAssemblyScript();
        var t1 = performance.now();
        appendListItem(assemblyScriptExecutionList, (t1-t0), result);

        if(isTryingToStop){
            i = executionCount;
        }
    }
}

function startWorkForCpp(executionCount) {
    console.log("START CPP");
    for (let i = 0; i < executionCount; i++) {

        var t0 = performance.now();
        let result = Module.monteCarloPi();
        var t1 = performance.now();
        appendListItem(cppExecutionList, (t1-t0), result);
        
        if(isTryingToStop){
            i = executionCount;
        }
    }
}

async function startWorkForRust(executionCount) {
    console.log("START RUST");
    for (let i = 0; i < executionCount; i++)
    {
        var t0 = performance.now();
        let result = calculatePiRust();
        var t1 = performance.now();
        appendListItem(rustExecutionList, (t1-t0), result);

        if(isTryingToStop) {
            i = executionCount;
        }
    }
}

function appendListItem(listElement, time, result) {
    var li = document.createElement("li");
    li.appendChild(document.createTextNode(`${time}ms\t${result}`));
    listElement.appendChild(li);
}
