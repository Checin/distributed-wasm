﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DistributedWasmConsole.Models;

namespace DistributedWasmConsole.Helpers
{
    public static class SolutionQualityHelper
    {
        public static int GetUsedVerticesPercentage(IEnumerable<Connection> solution, Graph graph)
        {
            var verticesUsedCount = solution.Select(c => c.To).Distinct().Count();

            return verticesUsedCount * 100 / graph.Vertices.Length;
        }
    }
}
