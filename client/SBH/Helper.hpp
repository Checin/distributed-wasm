  
#ifndef HELPER_HPP
#define HELPER_HPP

#include <iterator>
#include <thread>
#include <chrono>
#include <random>

// chyba lepiej bedzie podmienic na randa
namespace Helper
{
    inline int GenerateRandomNumber(int min, int max)
    {
        return rand() % (max - min + 1) + min;
    }
}

#endif /* HELPER_HPP */