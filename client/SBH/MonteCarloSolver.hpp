#ifndef SBH_MONTE_CARLO_SOLVER_HPP
#define SBH_MONTE_CARLO_SOLVER_HPP

#include "Helper.hpp"
#include "Graph.hpp"


namespace SBH
{
    class MonteCarloSolver
    {
    public:
        static Connection GetNextConnection(const std::shared_ptr<Vertex>& currentVertex, int missingLettersCount,
                                            int iterationCount, const std::vector<int> &usedArray, int maxDepth);
        static std::vector<Connection> GetSolution(std::shared_ptr<Graph>& graph, int interationCount, int maxDepth);

    private:
        MonteCarloSolver(Connection firstConnection, int missingLettersCount,
                         const std::vector<int> &usedArray, int maxDepth, int seed);
        static int CurrentUsageCount(const std::vector<int> &usedArray, const std::shared_ptr<Vertex> &vertexPtr);
        int GetScore(int iterationCount) const;
        int GetSingleSearchScore() const;

    private:
        const Connection firstConnection_;
        const int missingLettersCount_;
        const std::vector<int> &usedArray_;
        const int maxDepht_;
    };
} // namespace SBH

#endif // SBH_MONTE_CARLO_SOLVER_HPP