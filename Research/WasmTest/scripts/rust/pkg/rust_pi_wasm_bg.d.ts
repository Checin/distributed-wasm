/* tslint:disable */
/* eslint-disable */
export const memory: WebAssembly.Memory;
export function monte_carlo_js_rand(): number;
export function monte_carlo_rust_rand(): number;
export function __wbindgen_exn_store(a: number): void;
