﻿using DistributedTaskSolving.Application.Shared.Generics.Dto;

namespace DistributedTaskSolving.Application.Shared.Business.JobSystem.WorkUnits.Dto
{
    public class WorkUnitClientDto
    {
        public string UserAgent { get; set; }
        public string RamSize { get; set; }
    }
}