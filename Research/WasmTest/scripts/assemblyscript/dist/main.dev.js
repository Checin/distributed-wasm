"use strict";

var importObj = {
  module: {},
  env: {
    memory: new WebAssembly.Memory({
      initial: 256
    }),
    table: new WebAssembly.Table({
      initial: 0,
      element: 'anyfunc'
    }),
    abort: function abort() {
      return null;
    }
  }
};

var _exports;

request = new XMLHttpRequest();
request.open('GET', './scripts/assemblyscript/main.wasm');
request.responseType = 'arraybuffer';
request.send();

request.onload = function () {
  console.log(request.response);
  var bytes = request.response;
  WebAssembly.instantiate(bytes, importObject).then(function (results) {
    console.log(results);
    _exports = results.instance.exports;
  });
};

function calculatePiAssemblyScript() {
  return regeneratorRuntime.async(function calculatePiAssemblyScript$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return regeneratorRuntime.awrap(_exports.calculatePi());

        case 2:
          return _context.abrupt("return", _context.sent);

        case 3:
        case "end":
          return _context.stop();
      }
    }
  });
}