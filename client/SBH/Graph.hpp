#ifndef SBH_GRAPH_HPP
#define SBH_GRAPH_HPP

#include <memory>
#include <vector>

#include "Connection.hpp"
#include "Vertex.hpp"

namespace SBH
{
    class Vertex;
    class Graph
    {
    public:
        static std::shared_ptr<Graph> FromSplitSequence(const std::vector<std::string>& sequence, int targetLength, std::string startingVertex);
        const std::vector<std::shared_ptr<Vertex>>& GetVertices() const;
        const std::shared_ptr<Vertex>& GetStartVertex() const;
        int GetTargetLength() const;


    private:
        std::vector<std::shared_ptr<Vertex>> vertices_;
        std::shared_ptr<Vertex> startVertex_;
        int targetLength_;
    };
} // namespace SBH
#endif // SBH_GRAPH_HPP