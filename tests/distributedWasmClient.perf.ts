import { step, TestSettings } from '@flood/element'

export const settings: TestSettings = {
	userAgent: 'droplet-chromium-test',
	loopCount: 1,
	waitUntil: 'visible',
}

export default () => {
	step('Start', async browser => {
		await browser.visit('https://distributed-wasm.szymonchecinski.pl/client');
	})

	step('Calculate', async browser => {
		await browser.wait(99999999);
	})
}
