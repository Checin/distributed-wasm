
var exports;
request = new XMLHttpRequest();
request.open('GET', './scripts/assemblyscript/main.wasm');
request.responseType = 'arraybuffer';
request.send();

request.onload = function() {
  var bytes = request.response;
  WebAssembly.instantiate(bytes,  {
    module: {},
    env: {
      memory: new WebAssembly.Memory({ initial: 256 }),
      table: new WebAssembly.Table({ initial: 0, element: 'anyfunc' }),
      abort: () => null,
    }
  }).then(results => {
    console.log(results);
    exports = results.instance.exports;
  });
};

async function calculatePiAssemblyScript(){
  return await exports.calculatePi();
}