﻿using System;
using System.Linq;

namespace DistributedWasmConsole.Models
{
    public class Vertex
    {
        public long Id { get; set; }

        public Graph Graph { get; set; }

        public string Atom { get; set; }

        public Connection[] Connections { get; set; }

        public int ScoreFrom(Vertex from)
        {
            int i;
            var fromAtom = from.Atom;
            var atomLength = fromAtom.Length;
            var toAtom = Atom;

            for (i = 1; !toAtom.StartsWith(fromAtom.Substring(i)); ++i) ;

            return atomLength - i;
        }

        public void SetConnections()
        {
            Connections = Graph.Vertices.Select(v => new Connection(this, v, v.ScoreFrom(this))).Where(c => c.Score > 0).ToArray();
        }

        public override string ToString()
        {
            return Atom;
        }
    }
}
