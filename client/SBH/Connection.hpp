#ifndef SBH_CONNECTION_HPP
#define SBH_CONNECTION_HPP

#include <iostream>
#include <memory>
#include "Vertex.hpp"


namespace SBH
{
    class Vertex;
    class Connection
    {
    public:
        Connection() = default;
        Connection(const std::shared_ptr<Vertex>& from, const std::shared_ptr<Vertex>& to, int score)
            : from_(from), to_(to), score_(score)
        {}

        inline int Score() const noexcept
        {
            return score_;
        }

        inline const std::shared_ptr<Vertex>& To() const
        {
            return to_;
        }

        inline const std::shared_ptr<Vertex>& From() const
        {
            return from_;
        }

        inline const std::shared_ptr<Vertex> To2() const
        {
            return to_;
        }

        inline const std::shared_ptr<Vertex> From2() const
        {
            return from_;
        }

        friend std::ostream &operator<<(std::ostream &os, const Connection &connection)
        {
            os  << connection.From() 
                << " => " 
                << connection.To() 
                << " (" 
                << connection.Score() << ")";
            return os;
        }

    private:
        std::shared_ptr<Vertex> from_;
        std::shared_ptr<Vertex> to_;
        long score_;
    };

    using ConnectionPtr = std::shared_ptr<Connection>;
    using Connections = std::vector<Connection>;
} // namespace SBH

#endif // SBH_CONNECTION_HPP
