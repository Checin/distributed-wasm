﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace DistributedTaskSolving.Business.BusinessEntities.JobSystem.JobTypes
{
    public static class JobTypeEnums
    {
        public const string PasswordBruteForcing = "PasswordBruteForcing";
        public const string MonteCarlo = "MonteCarlo";
        public const string Sequencing = "Sequencing";
    }
}