var Module = {
    wasmMemory: new WebAssembly.Memory({initial:400, maximum:1000})
};

importScripts('main.js');

onmessage = function (e) {

    var stringVector = new Module.StringVector();

    e.data.vertices.forEach(element => {
        stringVector.push_back(element);
    });

    var resultVector = Module.calculate(e.data.startingVertex, e.data.targetLength, stringVector);

    stringVector.delete();

    var result = [];

    for (var i = 0; i < resultVector.size(); i ++) {
        var connection = resultVector.get(i);
        var from = connection.From2();
        var to = connection.To2();
        result.push({
            From: {
                Atom: from.Atom,
            },
            To: {
                Atom: to.Atom,
            },
            Score: connection.Score()
        });

        from.delete();
        to.delete();
        connection.delete();
    }
    resultVector.delete();

    postMessage(result);
}