using System;

namespace MonteCarlo
{
    public class Program
    {
        public static void Main()
        {
            CalculatePi();
        }

        public static float CalculatePi()
        {
            var inner = 0;
            var count = 0;
            var random = new Random();

            for (var i = 0; i < 300000000; i ++)
            {
                var x = random.NextDouble();
                var y = random.NextDouble();

                var distance = Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2));

                if (distance <= 1) {
                    inner++;
                }
                count ++;
            }

            return 4.0f * inner / (float)count;
        }
    }
}