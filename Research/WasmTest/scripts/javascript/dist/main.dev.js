"use strict";

function calculatePiJavascript() {
  var inner = 0;
  var count = 0;

  for (var i = 0; i < 300000000; i++) {
    var x = Math.random();
    var y = Math.random();
    var distance = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));

    if (distance <= 1) {
      inner++;
    }

    count++;
  }

  return 4 * inner / count;
}