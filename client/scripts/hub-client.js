const jobTypeName = 'Sequencing'; // Required
const algorithmName = null; // Optional and for analytics purposes
const programmingLanguage = null; // Optional and for analytics purposes
const serverUrl = 'https://distributed-task-solving.herokuapp.com';

var currentWorkUnitId;
var currentInstanceId;
var currentInstance;
var currentData;

var workUnitStartTime;
var workUnitFinishTime;

var stateEl = document.getElementById('state');
var dataEl = document.getElementById('data');

stateEl.innerHTML = 'Initializing...';

var calculationWorker = new Worker("./worker.js");

const connection = new signalR.HubConnectionBuilder()
    .withUrl(serverUrl + '/jobInstancesHub')
    .configureLogging(signalR.LogLevel.Information)
    .build();

function connect() {
    connection.start().then(function(){
        stateEl.innerHTML = 'Connected';
        startWorkUnit();
    }).catch(function (err) {
        stateEl.innerHTML = 'Initialization error';
        dataEl.innerHTML = err.toString();
        setTimeout(() => connect(), 5000);
        return console.error(err.toString());
    });
}

connection.onclose(() => {
    console.log("connection closed, retrying...");
    connect();
});

connect();

connection.on('ReceiveWorkUnit', function (workUnitId, data, instanceId) {
    currentWorkUnitId = workUnitId;
    currentData = data;

    if (currentInstanceId != instanceId) {
        fetch(serverUrl + '/api/jobInstances?id=' + instanceId, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => response.json())
        .then(instance => {
            currentInstance = JSON.parse(instance.key);
            currentInstanceId = instance.id;
            instanceIdChanged = true;
            workOnWorkUnit();
        });
    } else {
        instanceIdChanged = false;
        workOnWorkUnit();
    }
});

function workOnWorkUnit() {
    console.log(`Received work unit with data: ${currentData} (id: ${currentWorkUnitId})`);
    stateEl.innerHTML = 'Calculation in progress';
    dataEl.innerHTML = `WorkUnitId: ${currentWorkUnitId}<br/>starting vertex: ${currentData}<br/>target length: ${currentInstance.TargetLength}<br/>vertices: ${currentInstance.Vertices.join(', ')}`;
    workUnitStartTime = performance.now();
    calculationWorker.onmessage = function(e) {
        finishWorkUnit(currentWorkUnitId, e.data);
    }

    calculationWorker.postMessage({
        startingVertex: currentData,
        targetLength: currentInstance.TargetLength,
        vertices: currentInstance.Vertices
    });
}

function startWorkUnit() {
    stateEl.innerHTML = 'Connected, waiting for work unit';
    connection.invoke('StartWorkOnJobType', jobTypeName, {
        userAgent: navigator.userAgent,
        ramSize: ''
    }, algorithmName, programmingLanguage);
}

function finishWorkUnit(workUnitId, result) {
    workUnitFinishTime = performance.now();
    calculationWorker.terminate();
    calculationWorker = new Worker("./worker.js");
    var data = JSON.stringify(result);
    var sequence = result[0].From.Atom;
    result.forEach(c => sequence += c.To.Atom.substring(c.Score));
    stateEl.innerHTML = 'Finished';
    dataEl.innerHTML = `WorkUnitId: ${workUnitId}<br/>starting vertex: ${currentData}<br/>target length: ${currentInstance.TargetLength}<br/>result: ${sequence}`;
    connection.invoke('FinishWorkUnit', workUnitId, data, true, jobTypeName, (workUnitFinishTime - workUnitStartTime));

    startWorkUnit();
}